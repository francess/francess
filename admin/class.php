<?php
    include('include/header.php');
    include('include/sidebar.php');
    include('data/class_model.php');
    
    $search = isset($_POST['search']) ? $_POST['search']: null;
    $class = $class->getclass($search);
?>
<div id="page-wrapper">

    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    <small>CLASS INFORMATION</small>
                </h1>
                <ol class="breadcrumb">
                    <li>
                        <i class="fa fa-dashboard"></i> <a href="index.php">Dashboard</a>
                    </li>
                    <li class="active">
                        CLASSES
                    </li>
                </ol>
            </div>
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="form-inline form-padding">
                    <form action="class.php" method="post">
                        <input type="text" class="form-control" name="search" placeholder="Search Class Info...">
                        <button type="submit" name="submitsearch" class="btn btn-success"><i class="fa fa-search"></i> Search</button>                                
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addclass">Add New Class</button>
                    </form>
                </div>
            </div>
        </div>
        <!--/.row -->
        <hr />   
        <div class="row">
            <div class="col-lg-12">
                <?php if(isset($_GET['r'])): ?>
                    <?php
                        $r = $_GET['r'];
                        if($r=='added'){
                            $classs='success';   
                        }else if($r=='updated'){
                            $classs='info';   
                        }else if($r=='deleted'){
                            $classs='danger';   
                        }else{
                            $classs='hide';
                        }
                    ?>
                    <div class="alert alert-<?php echo $classs?> <?php echo $classs; ?>">
                        <strong>Class info successfully <?php echo $r; ?>!</strong>    
                    </div>
                <?php endif; ?>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th >Subject Code</th>
                                <th>Class Name</th>
                                <th>Semester</th>
                                <th>S.Y.</th>
                                <th>Teacher</th>
                                <th>Students</th>
                                <th>Edit</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $c = 1; ?>
                            <?php while($row = mysql_fetch_array($class)): ?>                            
                                <tr>
                                    <td><?php echo $c;?></td>
                                    <td><?php echo $row['subject'];?></td>
                                    <td><?php echo $row['course'].' '.$row['year'].' - '.$row['section'];?></td>
                                    <td><?php echo $row['sem'];?></td>                                
                                    <td><?php echo $row['sy'];?></td>                                
                                    <td><a href="classteacher.php?classid=<?php echo $row['id'];?>&teacherid=<?php echo $row['teacher'];?>" title="update teacher">View</a></td>
                                    <td><a href="classstudent.php?classid=<?php echo $row['id'];?>" title="update students" title="add student">View</a></td>
                                    <td>                                                                               
                                        <a href="edit.php?type=class&id=<?php echo $row['id']?>" title="Update Class"><i class="fa fa-edit fa-2x text-primary"></i></a>
                                        </td>
                                </tr>
                            <?php $c++; ?>
                            <?php endwhile; ?>
                            <?php if(mysql_num_rows($class) < 1): ?>
                                <tr>
                                    <td colspan="7" class="bg-danger text-danger text-center">*** EMPTY ***</td>
                                </tr>
                            <?php endif; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>
    <!-- /.container-fluid -->

</div>
<!-- /#page-wrapper -->    
<?php include('include/modal.php'); ?>
<?php include('include/footer.php'); ?>