<?php
    include('include/header.php');
    include('include/sidebar.php');
    include('data/student_model.php');
    
    $search = isset($_POST['search']) ? $_POST['search']: null;
    $student = $student->getstudent($search);
?>
<div id="page-wrapper">

    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    <small>STUDENTS LIST</small>
                </h1>
                <ol class="breadcrumb">
                    <li>
                        <i class="fa fa-dashboard"></i> <a href="index.php">Dashboard</a>
                    </li>
                    <li class="active">
                        STUDENTS
                    </li>
                </ol>
            </div>
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="form-inline form-padding">
                    <form action="studentlist.php" method="post">
                        <input type="text" class="form-control" name="search" placeholder="Search Students...">
                        <button type="submit" name="submitsearch" class="btn btn-success"><i class="fa fa-search"></i> Search</button>                                          
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addstudent"><i class="fa fa-user"></i> Add New Student</button>
                    </form>
                </div>
            </div>
        </div>
        <!--/.row -->
        <hr />   
        <div class="row">
            <div class="col-lg-12">
                <?php if(isset($_GET['r'])): ?>
                    <?php
                        $r = $_GET['r'];
                        if($r=='added'){
                            $class='success';   
                        }else if($r=='updated'){
                            $class='info';   
                        }else if($r=='deleted'){
                            $class='danger';   
                        }else if($r=='added an account'){
                            $class='success';   
                        }else if($r=='already have an account'){
                            $class='info';   
                        }else{
                            $class='hide';
                        }
                    ?>
                    <div class="alert alert-<?php echo $class?> <?php echo $classs; ?>">
                        <strong>Student <?php echo $r; ?>!</strong>    
                    </div>
                <?php endif; ?>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Student ID</th>
                                <th>Program Code</th>
                                <th>Firstname</th>
                                <th>Lastname</th>
								<th>Middlename</th>
								<th>Age</th>
								<th>Address</th>
								<th>Email Address</th>
                                <th class="text-center">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $c = 1; ?>
                            <?php while($row = mysql_fetch_array($student)): ?>                            
                                <tr>
                                    <td><?php echo $c;?></td>
                                    <td><a href="edit.php?type=student&id=<?php echo $row['id']?>"><?php echo $row['studid'];?></a></td>
                                    <td><?php echo $row['progCode'];?></td>
                                    <td><?php echo $row['fname'];?></td>
                                    <td><?php echo $row['lname'];?></td>
				<td><?php echo $row['mname'];?></td>
                                    <td><?php echo $row['age'];?></td>
				<td><?php echo $row['address'];?></td>
                                    <td><?php echo $row['email'];?></td>
                                    <td class="text-center">
                                        <a href="data/settings_model.php?q=addaccount&level=student&id=<?php echo $row['id']?>" title="Create Account" class="confirmacc"><i class="fa fa-key fa-2x text-warning"></i></a>
                                        <a href="studentsubject.php?id=<?php echo $row['id'];?>" title="Student Loads"><i class="fa fa-bar-chart-o fa-2x text-success"></i></a> &nbsp;
					<a href="edit.php?type=student&id=<?php echo $row['id']?>" title="Update Subject"><i class="fa fa-edit fa-2x text-primary"></i></a>
                                        </td>
                                </tr>
                            <?php $c++; ?>
                            <?php endwhile; ?>
                            <?php if(mysql_num_rows($student) < 1): ?>
                                <tr>
                                    <td colspan="5" class="bg-danger text-danger text-center">*** EMPTY ***</td>
                                </tr>
                            <?php endif; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>
    <!-- /.container-fluid -->

</div>
<!-- /#page-wrapper -->    
<?php include('include/modal.php'); ?>
<?php include('include/footer.php'); ?>