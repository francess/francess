<?php
    include('include/header.php');
    include('include/sidebar.php');
    include('data/subject_model.php');
    include('data/student_model.php');

    $mysubject = $subject->getallsubject($id); 
    $classid = isset($_GET['classid']) ? $_GET['classid'] : null;    
    $search = isset($_POST['search']) ? $_POST['search'] : null;    
    if(isset($_POST['search'])){
        $classid = $_POST['subject'];   
        $mystudent = $student->getstudentbysearch($classid,$search);
    }else{
        $mystudent = $student->getstudentbyclass($classid);
    }
?>
<div id="page-wrapper">

    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    <small>MY STUDENTS</small>
                </h1>
                <ol class="breadcrumb">
                    <li>
                        <i class="fa fa-dashboard"></i> <a href="index.php">Dashboard</a>
                    </li>
                    <li>
                        <a href="subject.php">My Subjects</a>
                    </li>
                    <li class="active">
                        Students
                    </li>
                </ol>
            </div>
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="form-inline form-padding">
                    <form action="student.php?classid=<?php echo $classid?>" method="post">
                        <input type="text" class="form-control" name="search" placeholder="Search by ID or Name">
                        <select name="subject" class="form-control" required>
                            <option value="">Select Subject...</option>                            
                            <?php while($row = mysql_fetch_array($mysubject)): ?>
                                <option value="<?php echo $row['id']?>" <?php if($row['id']==$classid) echo 'selected'; ?>><?php echo $row['subject'];?></option>
                            <?php endwhile; ?>
                        </select>
                        <button type="submit" name="submit" class="btn btn-primary"><i class="fa fa-search"></i> Search</button>                       
                        <a href="print.php?classid=<?php echo $classid; ?>" target="_blank"><button type="button" name="submit" class="btn btn-success"><i class="fa fa-print"></i> Print</button></a>            
                    </form>
                </div>
            </div>
        </div>
        <hr />
        <div class="row">
            <div class="col-lg-12">  
                          
            	<script type="text/javascript">
	$(document).ready(function($)
{
	console.log();
	//ajax row data
	var ajax_data =
	[
		{fname:"Code", lname:"With Mark", email:"mark@codewithmark.com"}, 
		{fname:"Mary", lname:"Moe", email:"mary@gmail.com"},
		{fname:"John", lname:"Doe", email:"john@yahoo.com"},
		{fname:"Julie", lname:"Dooley", email:"julie@gmail.com"},
	];



	var random_id = function() 
	{
		var id_num = Math.random().toString(9).substr(2,3);
		var id_str = Math.random().toString(36).substr(2);
		
		return id_num + id_str;
	};


	//--->create data table > start
	var tbl = '';
	tbl +='<table class="table table-hover">';

		//--->create table header > start
		tbl +='<thead>';
			tbl +='<tr>';
			tbl +='<th>First Name</th>';
			tbl +='<th>Last Name</th>';
			tbl +='<th>Email</th>';
			tbl +='<th>Options</th>';
			tbl +='</tr>';
		tbl +='</thead>';
		//--->create table header > end

		
		//--->create table body > start
		tbl +='<tbody>';

			//--->create table body rows > start
			$.each(ajax_data, function(index, val) 
			{
				//you can replace with your database row id
				var row_id = random_id();

				//loop through ajax row data
				tbl +='<tr row_id="'+row_id+'">';
					tbl +='<td ><div class="row_data" edit_type="click" col_name="fname">'+val['fname']+'</div></td>';
					tbl +='<td ><div class="row_data" edit_type="click" col_name="lname">'+val['lname']+'</div></td>';
					tbl +='<td ><div class="row_data" edit_type="click" col_name="email">'+val['email']+'</div></td>';

					//--->edit options > start
					tbl +='<td>';
					 
						tbl +='<span class="btn_edit" > <a href="#" class="btn btn-link " row_id="'+row_id+'" > Edit</a> </span>';

						//only show this button if edit button is clicked
						tbl +='<span class="btn_save"> <a href="#" class="btn btn-link"  row_id="'+row_id+'"> Save</a> | </span>';
						tbl +='<span class="btn_cancel"> <a href="#" class="btn btn-link" row_id="'+row_id+'"> Cancel</a> | </span>';

					tbl +='</td>';
					//--->edit options > end
					
				tbl +='</tr>';
			});

			//--->create table body rows > end

		tbl +='</tbody>';
		//--->create table body > end

	tbl +='</table>';	
	//--->create data table > end

	//out put table data
	$(document).find('.tbl_user_data').html(tbl);

	$(document).find('.btn_save').hide();
	$(document).find('.btn_cancel').hide(); 


	//--->make div editable > start
	$(document).on('click', '.row_data', function(event) 
	{
		event.preventDefault(); 

		if($(this).attr('edit_type') == 'button')
		{
			return false; 
		}

		//make div editable
		$(this).closest('div').attr('contenteditable', 'true');
		//add bg css
		$(this).addClass('bg-warning').css('padding','5px');

		$(this).focus();
	});	
	//--->make div editable > end


	//--->save single field data > start
	$(document).on('focusout', '.row_data', function(event) 
	{
		event.preventDefault();

		if($(this).attr('edit_type') == 'button')
		{
			return false; 
		}

		var row_id = $(this).closest('tr').attr('row_id'); 
		
		var row_div = $(this)				
		.removeClass('bg-warning') //add bg css
		.css('padding','')

		var col_name = row_div.attr('col_name'); 
		var col_val = row_div.html(); 

		var arr = {};
		arr[col_name] = col_val;

		//use the "arr"	object for your ajax call
		$.extend(arr, {row_id:row_id});

		//out put to show
		$('.post_msg').html( '<pre class="bg-success">'+JSON.stringify(arr, null, 2) +'</pre>');
		
	});	
	//--->save single field data > end

 
	//--->button > edit > start	
	$(document).on('click', '.btn_edit', function(event) 
	{
		event.preventDefault();
		var tbl_row = $(this).closest('tr');

		var row_id = tbl_row.attr('row_id');

		tbl_row.find('.btn_save').show();
		tbl_row.find('.btn_cancel').show();

		//hide edit button
		tbl_row.find('.btn_edit').hide(); 

		//make the whole row editable
		tbl_row.find('.row_data')
		.attr('contenteditable', 'true')
		.attr('edit_type', 'button')
		.addClass('bg-warning')
		.css('padding','3px');

		//--->add the original entry > start
		tbl_row.find('.row_data').each(function(index, val) 
		{  
			//this will help in case user decided to click on cancel button
			$(this).attr('original_entry', $(this).html());
		}); 		
		//--->add the original entry > end

	});
	//--->button > edit > end
	$(document).ready(function(){
		$('.edit').hide();

		$('.cell').click(function(e){
			var tbl_row = $(this).closest('tr');
			var editxx = $(this).closest('tr');
			var row_id = tbl_row.attr('row_id');
			e.preventDefault();
			console.log('clicked');
			editxx.find('.edit').attr('contenteditable', 'false').show();
		});

	});

	//--->button > cancel > start	
	$(document).on('click', '.btn_cancel', function(event) 
	{
		event.preventDefault();

		var tbl_row = $(this).closest('tr');

		var row_id = tbl_row.attr('row_id');

		//hide save and cacel buttons
		tbl_row.find('.btn_save').hide();
		tbl_row.find('.btn_cancel').hide();

		//show edit button
		tbl_row.find('.btn_edit').show();

		//make the whole row editable
		tbl_row.find('.row_data')
		.attr('edit_type', 'click')
		.removeClass('bg-warning')
		.css('padding',''); 

		tbl_row.find('.row_data').each(function(index, val) 
		{   
			$(this).html( $(this).attr('original_entry') ); 
		});  
	});
	//--->button > cancel > end

	
	//--->save whole row entery > start	
	$(document).on('click', '.btn_save', function(event) 
	{
		event.preventDefault();
		var tbl_row = $(this).closest('tr');

		var row_id = tbl_row.attr('row_id');

		
		//hide save and cacel buttons
		tbl_row.find('.btn_save').hide();
		tbl_row.find('.btn_cancel').hide();

		//show edit button
		tbl_row.find('.btn_edit').show();


		//make the whole row editable
		tbl_row.find('.row_data')
		.attr('edit_type', 'click')
		.removeClass('bg-warning')
		.css('padding',''); 

		//--->get row data > start
		var arr = {}; 
		tbl_row.find('.row_data').each(function(index, val) 
		{   
			var col_name = $(this).attr('col_name');  
			var col_val  =  $(this).html();
			arr[col_name] = col_val;
		});
		//--->get row data > end

		//use the "arr"	object for your ajax call
		$.extend(arr, {row_id:row_id});

		//out put to show
		$('.post_msg').html( '<pre class="bg-success">'+JSON.stringify(arr, null, 2) +'</pre>');
		 

	});
	//--->save whole row entery > end

console.log(tbl);
}); 
</script>

<div class="panel panel-default">
  <div class="panel-body">
	
	<!-- <div class="tbl_user_data"></div> -->

  </div>
</div>

                <div class="table-responsive">
                    <table class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th class="text-center">Student ID</th>
                                <th class="text-center">Name</th>
                                <th class="text-center">Prelims</th>
                                <th class="text-center">Midterms</th>
								<th class="text-center">Prefinals</th>
                                <th class="text-center">Finals</th>
                                <th class="text-center">Average</th>
                                <th class="text-center"></th>
                            </tr>
                        </thead>
                        <tbody>
                    <?php $c=1; ?>
					<?php foreach($mystudent as $row):
						$sid =  $row['studid'];
						?>
                        <tr>
                            <td><?php echo $c; ?></td>    
                            <td class="text-center"><?php echo $row['studid']; ?></td>    
                            <td class="text-center"><?php echo $row['lname'].', '.$row['fname']; ?></td>  
   							<?php $grade = $student->getstudentgrade($row['id'],$classid); ?>
							<td class="text-center row_data cell" contenteditable="true" edit_type="click"><?php echo
							 $grade['prelims'];?></td>    
							<td class="text-center row_data cell" contenteditable="true"><?php echo 
							$grade['midterms'];?></td>  
							<td class="text-center row_data cell" contenteditable="true"><?php echo 
							$grade['prefinals'];?></td>   
							<td class="text-center row_data cell" contenteditable="true"><?php
								echo 
							$grade['finals'];?></td>    
                            <td class="text-center" ><?php echo $grade['total'];?></td>  
                            <td class="text-center test-edit">
							
							<!-- <a href="calculate.php?studid=<?php echo $row['id']; ?>&classid=<?php echo $classid ?>" <i class="fa fa-edit fa-2x text-primary"></a> -->
							<?php 
							$q = "select * from student where studid='$sid'";
							$r = mysql_query($q);
							$sss="";
							
							   while($rowx = mysql_fetch_assoc($r)){
									$sss = $rowx['id'];    
								} 
								
							?>
							<a href="savedata.php?studid=<?php echo $sss;?>&classid=<?php echo $classid;?>&prelims=<?php echo $grade['prelims'];?>&midterms=<?php echo $grade['midterms'];?>&prefinals=<?php echo $grade['prefinals'];?>&finals=<?php echo $grade['finals'];?>" class="edit btn btn-primary"> save </a>

							
							</td>    
                        </tr>
                    <?php $c++; ?>
                    <?php endforeach; ?>
                    <?php if(!$mystudent): ?>
                        <tr><td colspan="8" class="text-center text-danger"><strong>*** No Result ***</strong></td></tr>
                    <?php endif; ?>
                        </tbody>
                    </table>
		
		

                </div>        
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->

</div>
<!-- /#page-wrapper -->    
<?php include('include/footer.php');