<?php 
    include('../config.php'); 
    $level = isset($_SESSION['level']) ? $_SESSION['level']: null;
    if($level == null){
        header('location:../index.php');
    }else if($level != 'student'){
        header('location:../'.$level.'');
    }
    
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="favicon.ico">

    <title>STI STUDENT PORTAL</title>

    <!-- Bootstrap core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="../css/font-awesome.min.css" />
    <link rel="stylesheet" href="../css/style.css" />
    <link rel="stylesheet" href="mystyle.css" />
    

  </head>

  <body>

    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="index.php">STI STUDENT PORTAL</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <div class="navbar-form navbar-right">
                <label class="text-primary">
                    Hi, <?php echo $_SESSION['name']; ?>&nbsp;&nbsp;
                </label>
                <a href="../logout.php"><button type="button" class="btn btn-success" name="submit">Logout</button></a>
                <button type="button" class="btn btn-info" data-toggle="modal" data-target="#changepass">Change Password</button>
            </div>
        </div><!--/.navbar-collapse -->
      </div>
    </nav>

    

<?php
    include('grade.php');
    $mysubject = $grade->getsubject();
?>
<div class="row">
            <div class="col-lg-12">
                <?php if(isset($_GET['msg'])): ?>
                    <?php if($_GET['msg']=='success'){
                            echo '
                                <div class="alert alert-success">
                                    <strong>Password Changed! Logout and Login with new password</strong>
                                </div>
                            ';
                        }else{
                             echo '
                                <div class="alert alert-danger">
                                    <strong>Password incorrect. Please try again!</strong>
                                </div>
                            ';
                        }
                        
                    ?>
                    <?php endif;?>
                    </div>
        </div>

    <div class="container" style="margin-top:60px;">
      <!-- Example row of columns -->
    <div class="row">
        <div class="col-lg-12">
            <h2 class="text-center">Report of Grades</h2>
            <div class="">
                <table class="table table-bordered">
                    <thead>
                        <tr class="alert alert-info">
                            <th class="text-center">Subject Code</th>
                            <th class="text-center">Subject Title</th>
                            <th class="text-center">Prelims</th>
                            <th class="text-center">Midterms</th>
			                <th class="text-center">Prefinals</th>
                            <th class="text-center">Finals</th>
                            <th class="text-center">Final Grade</th>
                            <th class="text-center">Equivalent</th>
                           <!-- <th class="text-center">Units</th>-->
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($mysubject as $row): ?>
                            <tr>
                                <td class="text-center"><?php echo $row['subject']; ?></td>
                                <?php $title = $grade->getsubjectitle($row['subject']);?>
                                <td class="text-center"><?php echo $title[0]['title']; ?></td>
                                <?php $mygrade = $grade->getgrade($row['id']); ?>
                                <td class="text-center"><?php echo $mygrade['prelims']; ?></td>
                                <td class="text-center"><?php echo $mygrade['midterms']; ?></td>
                                <td class="text-center"><?php echo $mygrade['prefinals']; ?></td>
                                <td class="text-center"><?php echo $mygrade['finals']; ?></td>
                                <td class="text-center"><?php echo $mygrade['total']; ?></td>
                                <td class="text-center"><?php echo $mygrade['eqtotal']; ?></td>
                               <!-- <td class="text-center"><?php echo $title[0]['unit']; ?></td>-->
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
                <h4 class="text-center text-danger">*** NOTHING FOLLOWS ***</h4>
            </div>
        </div>    
    </div>
   

<div class="modal fade" id="changepass" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
        <div class="modal-header">
            <h3>Change Password</h3>
        </div>
        <div class="modal-body">
            <form action="password.php?q=changepassword&username=<?php echo $_SESSION['id'];?>" method="post">
                <div class="form-group">
                    <input type="password" class="form-control" name="current" placeholder="Current Password" />
                </div>
                <div class="form-group">
                    <input type="password" class="form-control" name="new" placeholder="New Password" />
                </div>
                <div class="form-group">
                    <input type="password" class="form-control" name="confirm" placeholder="Confirm Password" />
                </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary"><i class="fa fa-plus"></i> Change</button>
            </form>
        </div>
    </div>
  </div>
</div>
        

      <hr>

	<footer class="navbar navbar-fixed-bottom" role="navigation">
    <div class="container text-center">
        <p>Copyright &copy; 2018 STI Student Portal | STI College Tuguegarao</p>
    </div>
</footer>
    </div> <!-- /container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="../js/jquery.js"></script>
    <script src="../js/bootstrap.min.js"></script>
  </body>
</html>
